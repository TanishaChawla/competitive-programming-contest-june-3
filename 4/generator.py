from random import choice
import os
import sys

args=sys.argv;
test_case_number=args[1]
folder_name=os.path.join("test_cases",test_case_number)
if not os.path.exists(folder_name):
    os.mkdir(folder_name)
t_range=range(1,10001)
nm_range=range(1,11)
i_range=range(1,1001)
t=choice(t_range)
arr=[]
arr.append(t)
for x in range(t):
    n=choice(nm_range)
    m=choice(nm_range)
    ar=[]
    ar.append(n)
    ar.append(m)
    arr.append(' '.join(map(str,ar)))
    for y in range(n):
        ar=[]
        for z in range(m):
            x=choice(i_range)
            ar.append(x)
        arr.append(' '.join(map(str,ar)))
open(os.path.join("test_cases",test_case_number,"in.txt"),'w').write('\n'.join(map(str,arr)))
