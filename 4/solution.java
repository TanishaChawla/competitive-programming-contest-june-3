import java.io.*;
import java.util.*;

class solution{
  public static void main(String args[])throws IOException{
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    for(int i=0;i<t;i++){
      int n=sc.nextInt();
      int m=sc.nextInt();
      int ar[][]=new int[n][m];
      for(int x=0;x<n;x++){
        for(int y=0;y<m;y++){
          ar[x][y]=sc.nextInt();
        }
      }
      int max=getMaximum(0,0,ar);
      System.out.println(max);
    }
  }
  public static int getMaximum(int x,int y,int ar[][])
  {
    int n=ar.length;
    int m=ar[0].length;
    int max1=0;
    int max2=0;
    if(x==(n-1) && y==(n-1)){
      return ar[x][y];
    }
    else{
      if(x<(n-1) && y<(m-1)){
        max1=getMaximum(x+1,y,ar);
        max2=getMaximum(x,y+1,ar);
      }
      else if(x<(n-1)){
        max1=getMaximum(x+1,y,ar);

      }
      else if(y<(m-1)){
        max2=getMaximum(x,y+1,ar);
      }
      return ar[x][y]+(max1>max2?max1:max2);
    }
  }
}
